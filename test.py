import unittest
import intpack


class Tests(unittest.TestCase):
    def test_pack_unpack(self):
        # pack and unpack some numbers
        self.assertEqual(intpack.unpack(intpack.pack(4, 5), 2), [4, 5])
        self.assertEqual(intpack.unpack(intpack.pack(6, 7, 8), 3), [6, 7, 8])

        # other way around is also fine
        self.assertEqual(intpack.pack(*intpack.unpack(123, 4)), 123)
        self.assertEqual(intpack.pack(*intpack.unpack(5678, 9)), 5678)

    def test_mpack_munpack(self):
        # pack and unpack some numbers
        self.assertEqual(intpack.munpack(intpack.mpack(4, 5)), [4, 5])
        self.assertEqual(intpack.munpack(intpack.mpack(6, 7, 8)), [6, 7, 8])
