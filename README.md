# intpack

Math teaches us that ℕ, ℕ×ℕ, ℕ×ℕ×ℕ, ... have the same cardinality.
Consequently bijective functions must exist between any of those.

This library implements one of those functions.

## But why?

Not particular reason. Just playing around with math. And connecting
mathematical theory to actual implementations.
