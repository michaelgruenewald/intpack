#!/usr/bin/env python
from setuptools import setup

setup(
    name='intpack',
    author='Michael Gruenewald',
    author_email='mail@michaelgruenewald.eu',
    description='integers to integer packing library',
    license='License :: OSI Approved :: MIT License',
    url='https://bitbucket.org/michaelgruenewald/intpack',
    version='0.1',
    py_modules=['intpack'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
)
