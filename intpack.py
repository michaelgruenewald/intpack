def pack(*vs):
    """ Pack the given positive integers into a single positive integer. """
    assert all(isinstance(x, int) and x >= 0 for x in vs)
    nums = list(vs)
    v = 0
    pos = 0
    while any(nums):
        for i in range(len(nums)):
            nums[i], bit = nums[i] // 2, nums[i] % 2
            v += bit << pos
            pos += 1
    return v


def unpack(v, count=2):
    """ Unpack the value v into 'count' integers. """
    assert isinstance(v, int) and v >= 0
    assert count > 0
    nums = [0] * count
    pos = 0
    while v:
        for i in range(count):
            v, bit = v // 2, v % 2
            nums[i] += bit << pos
        pos += 1
    return nums


def mpack(*vs):
    """ Helper to pack multiple integers. See `munpack`. """
    return pack(pack(*vs), len(vs))


def munpack(v):
    """ Helper to unpack multiple integers without specifying the count. """
    return unpack(*unpack(v, 2))
